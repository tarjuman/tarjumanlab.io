const form = document.getElementById('form');
const rangeInput = document.getElementById('days');

const priceValueElement = document.getElementById('price-value');
const resultValueElement = document.getElementById('result-value');
const descriptionValueElement = document.getElementById('description-value');

const addButton = document.getElementById('add-button');

const url = 'https://crwtnhczjcllfiyrtrwu.supabase.co/functions/v1/insert-subscription';
const deliveryOptions = ['Instant', 'Ultra Fast', 'Express', 'Fast', 'Rapid', 'Standard', 'Regular', 'Economy', 'Budget'];
const basePrice = 80000;
const decay = 27500;
const minDays = 1;
const maxDays = 9;

const getInputs = () => Object.fromEntries([...form.elements].filter(({ name }) => name).map(({ name, value }) => [name, value]));

const resetForm = () => {
  form.reset();
  document.querySelectorAll('.dropdown-container').forEach((container) => container.remove());
  rangeInput.value = maxDays;
};

const calculatePrice = () => {
  const dropdownContainers = document.querySelectorAll('.dropdown-container');
  const days = parseInt(rangeInput.value);
  const price = (basePrice + decay * (maxDays - days)) * dropdownContainers.length;
  priceValueElement.textContent = (Math.ceil(price / 10000) * 10000).toLocaleString();
};

const updateResult = () => {
  const currentValue = parseInt(rangeInput.value);
  const finishDate = new Date(Date.now() + currentValue * 24 * 60 * 60 * 1000);
  const formattedDate = finishDate.toLocaleDateString();
  const description = deliveryOptions[currentValue - 1];
  resultValueElement.textContent = formattedDate;
  descriptionValueElement.textContent = description;
};

const updateUI = () => {
  updateResult();
  calculatePrice();
};

form.addEventListener('submit', async (e) => {
  e.preventDefault();

  const orderNumber = Math.floor(1000000 + Math.random() * 9000000);

  const files = [...form.elements]
    .filter((input) => input.type === 'file')
    .map((input) => input.files[0]);

  const formData = new FormData(form);
  files.forEach((file) => formData.append('files', file));
  formData.append('order', orderNumber.toString());

  const response = await fetch(url, {
    method: 'POST',
    body: formData,
  });

  const result = await response.json();
  console.log(result);
  resetForm();
  const orderElement = document.createElement('p');
  orderElement.textContent = `Your order number is ${orderNumber}`;
  document.body.appendChild(orderElement);
  updateUI();
});


addButton.addEventListener('click', calculatePrice);
rangeInput.addEventListener('input', () => {
  updateUI();
});

updateUI();