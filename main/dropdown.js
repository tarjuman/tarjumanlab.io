let dropdownCount = 1;

function handleDropdownChange(event) {
  const select = event.target;
  const container = select.nextElementSibling;
  const value = select.value;

  if (value === 'others') {
    const userInput = prompt('Enter the type of the file:');
    container.innerHTML = `<input type="file" id="upload-${userInput.replace(/\s+/g, '_')}"><label>Upload ${userInput}</label>`;
  } else {
    container.innerHTML = `<input type="file" id="upload-${value}"><label>Upload ${value}</label>`;
  }
}

function deleteRow(event) {
  event.target.parentNode.remove();
  dropdownCount--;
}

function addDropdown() {
  const form = document.getElementById('form');
  const dropdownContainer = document.createElement('div');
  dropdownContainer.className = 'dropdown-container';
  dropdownContainer.innerHTML = `
            <p>${dropdownCount}:</p>
            <select>
              <option value="">Select an image</option>
              <option value="AktaKelahiran">Akta Kelahiran</option>
              <option value="IjazahSMA">Ijazah SMA</option>
              <option value="NilaiIjazahSMA">Nilai Ijazah SMA</option>
              <option value="others">Lainnya</option>
            </select>
            <div></div>
            <button type="button">x</button>
        `;
  const select = dropdownContainer.querySelector('select');
  const button = dropdownContainer.querySelector('button');

  select.addEventListener('change', handleDropdownChange);
  button.addEventListener('click', deleteRow);

  form.insertBefore(dropdownContainer, document.getElementById('add-button'));
  dropdownCount++;
}

document.getElementById('add-button').addEventListener('click', addDropdown);

document.getElementById('form').addEventListener('submit', (event) => {
  event.preventDefault();
  dropdownCount = 1;
});